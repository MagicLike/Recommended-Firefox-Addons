# Recommended Browser Addons
<a href="https://matrix.to/#/#Recommendations:matrix.org"><img alt="Matrix" height="20" src="https://shields.io/matrix/Recommendations:matrix.org?color=%23ffffff&label=Matrix&logo=Matrix"></a>

A list of recommended Librewolf/Firefox addons with a focus on privacy and open source.

## Table of contents

1. [Privacy](#privacy)
2. [Security](#security)
3. [Added features](#added-features)
4. [Special thanks](#special-thanks)
5. [License](#license)
6. [Legal notice](#legal-notice)

## Privacy
#### Adblocker
- [uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin)

#### Wide-spectrum controller		
- [uMatrix](https://addons.mozilla.org/en-US/firefox/addon/umatrix)

#### Fingerprinting
- [CanvasBlocker](https://addons.mozilla.org/en-US/firefox/addon/canvasblocker)

#### Local CDNs emulator
- [LocalCDN](https://addons.mozilla.org/en-US/firefox/addon/localcdn-fork-of-decentraleyes)

#### Redirects
- [Skip Redirect](https://addons.mozilla.org/en-US/firefox/addon/skip-redirect)

- [Redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector)

- [LibRedirect](https://addons.mozilla.org/en-US/firefox/addon/libredirect)

#### Referer
- [Smart Referer](https://addons.mozilla.org/en-US/firefox/addon/smart-referer)

<br>

## Security
- [KeePassXC-Browser](https://addons.mozilla.org/en-US/firefox/addon/keepassxc-browser)

- [Bitwarden](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager)

<br>

## Added features
- [Advanced Profanity Filter](https://addons.mozilla.org/firefox/addon/advanced_profanity_filter)

- [Dark Reader](https://addons.mozilla.org/firefox/addon/darkreader)

<br>

## Special thanks
- @pixelcode for the original `pixelcode/Recommended-Firefox-Addons`. This project was initially just a random fork of `pixelcode/Recommended-Firefox-Addons`!

- [Icon](https://commons.wikimedia.org/wiki/File:Jigsaw_piece_yellow_01.svg) by [Atón](https://commons.wikimedia.org/wiki/User:At%C3%B3n) under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en)

If you aren't on the above list, don't feel bad about it. We still give our regular thanks to you.

<br>

## License
This text is dedicated to public domain and is therefore licensed under the [CC0 1.0 License](https://creativecommons.org/publicdomain/zero/1.0).

## Legal notice

This repository is not affiliated with any of the mentioned addons. All addons belong to their respective owners.

This repository is not affiliated with Librewolf.

This repository is not affiliated with Firefox. Firefox is a trademark of the Mozilla Foundation in the U.S. and other countries.