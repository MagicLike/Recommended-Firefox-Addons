---
name: Legal stuff
about: Copyright, licenses, trademarks, DMCAs, takedowns, etc...
title: ""
labels: ["legal", "Priority: High"]
assignees: ""
---

<!-- Welcome to Recommendations! Write your issue directly involving legal compliance here (Markdown supported, learn more: https://www.markdownguide.org/). Please delete these three setences.-->
